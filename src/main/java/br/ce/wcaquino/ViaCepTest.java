package br.ce.wcaquino;

import org.hamcrest.Matchers;
import org.junit.Test;

import io.restassured.RestAssured;

public class ViaCepTest {
	
	
	//Faz um get no viacep e valida o endereco retornado
	@Test
	public void consultarCepEValidaRetorno() {
		RestAssured
		.given()
			.log().all()
			.pathParam("cep", 92425210)
		.when()
			.get("https://viacep.com.br/ws/{cep}/json/")
		.then()
			.log().all()
			.statusCode(200)
			.body("localidade", Matchers.is("Canoas"))
			.body("logradouro", Matchers.containsString("G. Reis"))
			.body("bairro", Matchers.is("São José"))
			.body("uf", Matchers.is("RS"))
		;
	}
	
	@Test
	public void consultaClimaPorCepEPostalCode() {		
		RestAssured
		.given()
			.log().all()
			.pathParam("cep", "92425")
			.pathParam("postalCode", "br")
			.pathParam("key", "5a02ac512b60f780eb43679c0758e65d")
		.when()
			.get("https://samples.openweathermap.org/data/2.5/weather?zip={cep},{postalCode}&appid={key}")
		.then()
			.log().all()
			.statusCode(200)
		;
	}

}

//chave 
//5a02ac512b60f780eb43679c0758e65d
