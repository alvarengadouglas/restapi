package br.ce.wcaquino;

import static org.junit.Assert.assertEquals;

import org.hamcrest.Matchers;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;

public class OlaMundoTest {
	//primeiro teste com rest assured validando status code
	@Test
	public void olaMundo() {
		Response response = RestAssured.request(Method.GET, "http://restapi.wcaquino.me/ola");
		System.out.println(response.getBody().asString());
		System.out.println(response.statusCode());
		assertEquals(200, response.getStatusCode());
	
	}
	
	//nova forma de fazer um request
	@Test
	public void outraFormaDeFazerRequest() {
		RestAssured
		.given()
		.when()
			.get("http://restapi.wcaquino.me/ola")
		.then()
			.statusCode(200);
		
	}
	
	//resquest validando resposta no corpo
	@Test
	public void devoValidarBody() {	
		RestAssured
		.given()
		.when()
			.get("http://restapi.wcaquino.me/ola")
		.then()
			.statusCode(200)
			.body(Matchers.is("Ola Mundo!"));
	}

}