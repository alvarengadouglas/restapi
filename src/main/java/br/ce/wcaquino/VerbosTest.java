package br.ce.wcaquino;

import java.util.HashMap;
import java.util.Map;

import org.hamcrest.Matchers;
import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

public class VerbosTest {
	
	//post para cadastrar usuario
	@Test
	public void criarUsuarioComPost() {
		RestAssured
		.given()
			.log().all()
			.contentType("application/json")
			.body("{\"name\": \"Douglas\",\"age\": 24}")
		.when()
			.post("http://restapi.wcaquino.me/users")
		.then()
			.log().all()
			.statusCode(201)
			.body("id", Matchers.notNullValue())
			.body("name", Matchers.is("Douglas"))
			.body("age",Matchers.is(24))
		;
	}
	
	//post para validar que usuario nao é criado sem o atributo name.s
	@Test
	public void naoDeveCriarUsuarioSemNome() {
		RestAssured
		.given()
			.log().all()
			.contentType("application/json")
			.body("{\"age\": 24}")
		.when()
			.post("http://restapi.wcaquino.me/users")
		.then()
			.log().all()
			.statusCode(400)
			.body("id", Matchers.nullValue())
			.body("error", Matchers.is("Name é um atributo obrigatório"))
		;
	}
	
	//cria um usuario com verbo post atraves de um XML
	@Test
	public void criarUsuarioComXML() {
		RestAssured
		.given()
			.log().all()
			.contentType(ContentType.XML) 
			.body("<user><name>Douglas</name><age>24</age></user>")
		.when()
			.post("http://restapi.wcaquino.me/usersXML")
		.then()
			.log().all()
			.statusCode(201)
			.body("user.name", Matchers.is("Douglas"))
			.body("user.age", Matchers.is("24"))
		;
	}
	
	//altera um usuario cadastrado com o verbo PUT
	@Test
	public void alterarUmUsuarioComPut() {
		RestAssured
		.given()
			.log().all()
			.contentType(ContentType.JSON)
			.body("{\"name\": \"Douglas\",\"age\": 24}")
		.when()
			.put("http://restapi.wcaquino.me/users/1")
		.then()
			.log().all()
			.statusCode(200)
			.body("id", Matchers.is(1))
			.body("name", Matchers.is("Douglas"))
			.body("age",Matchers.is(24))
			.body("salary", Matchers.is(1234.5678f))
		;
	}
	
	//altera um usuario passando uma URI paramertizavel
	@Test
	public void alterarUsuarioComURLParametrizavel() {
		RestAssured
		.given()
			.log().all()
			.contentType(ContentType.JSON)
			.body("{\"name\": \"Douglas\",\"age\": 24}")
		.when()
			.put("http://restapi.wcaquino.me/{entidade}/{userId}", "users", "1")
		.then()
			.log().all()
			.statusCode(200)
			.body("id", Matchers.is(1))
			.body("name", Matchers.is("Douglas"))
		;
	}
	
	//altera um usuario passando uma URI paramertizavel
	@Test
	public void alterarUsuarioComURLParametrizavel2() {
		RestAssured
		.given()
			.log().all()
			.contentType(ContentType.JSON)
			.body("{\"name\": \"Douglas\",\"age\": 24}")
			.pathParam("entidade", "users")
			.pathParam("userId", "1")
		.when()
			.put("http://restapi.wcaquino.me/{entidade}/{userId}")
		.then()
			.log().all()
			.statusCode(200)
			.body("name", Matchers.is("Douglas"))
		;
	}
	
	//Remove um usuario e valida o status retornado
	@Test
	public void removerUsuario() {
		RestAssured
		.given()
			.log().all()
		.when()
			.delete("http://restapi.wcaquino.me/users/1")
		.then()
			.log().all()
			.statusCode(204)
		;
	}
	
	//Testa removere um usuario inexistente e valida mensagem
	@Test
	public void naoDeveRemoverUsuarioInexistente() {
		RestAssured
		.given()
			.log().all()
		.when()
			.delete("http://restapi.wcaquino.me/users/1000")
		.then()
			.statusCode(400)
			.body("error", Matchers.is("Registro inexistente"))
		;
	}
	
	//cria um usuario atraves de MAP e nao passando o json diretamente no corpo do body.
	@Test
	public void criandoUmUsuarioComMAP() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", "Usuario via MAP");
		params.put("age", 24);
		
		RestAssured
		.given()
			.log().all()
			.contentType(ContentType.JSON)
			.body(params)
		.when()
			.post("http://restapi.wcaquino.me/users")
		.then()
			.log().all()
			.statusCode(201)
			.body("id", Matchers.notNullValue())
			.body("name", Matchers.is("Usuario via MAP"))
			.body("age", Matchers.is(24))
		;
	}
	
	//cria um usuario passando um objeto
	@Test
	public void criandoUmUsuarioComObjeto() {
		User user = new User("Usando Objeto", 25);
		
		RestAssured
		.given()
			.log().all()
			.contentType(ContentType.JSON)
			.body(user)
		.when()
			.post("http://restapi.wcaquino.me/users")
		.then()
			.log().all()
			.statusCode(201)
			.body("id", Matchers.notNullValue())
			.body("name", Matchers.is("Usando Objeto"))
			.body("age", Matchers.is(25))
		;
	}

}
