package br.ce.wcaquino;

import org.hamcrest.Matchers;
import org.junit.BeforeClass;
import org.junit.Test;

import io.restassured.RestAssured;

public class UserXMLTest {
	
	//Configura a URI base, porta e base path
	@BeforeClass
	public static void setUp() {
		RestAssured.baseURI = "http://restapi.wcaquino.me";
//		RestAssured.port = 443;
//		RestAssured.basePath = "";
	}

	//faz um get no usuario 3 e valida name, age e filhos
	@Test
	public void getComXML() {
		RestAssured
		.given()
		.when()
			.get("/usersXML/3")
		.then()
			.body("user.name", Matchers.is("Ana Julia"))
			.body("user.age", Matchers.is("20"))
			.body("user.filhos.name", Matchers.hasItems("Zezinho", "Luizinho"))
			.body("user.filhos.name[0]", Matchers.is("Zezinho"))
			.body("user.filhos.name[1]", Matchers.is("Luizinho"))
			.body("user.filhos.name.size()", Matchers.is(2))
	
		;
	}
	
	// faz um get na API e busca dados no body com uma pesquisa avancada para validacao.
	@Test
	public void getComXmlAvancado() {
		RestAssured
		.given()
		.when()
			.get("/usersXML/")
		.then()
			.statusCode(200)
			.body("users.user.size()", Matchers.is(3))
			.body("users.user.findAll{it.age.toInteger() <= 25}.size()", Matchers.is(2))
			.body("users.user.@id", Matchers.hasItems("1","2","3"))
			.body("users.user.find{it.age == 25}.name", Matchers.is("Maria Joaquina"))
			.body("users.user.findAll{it.name.toString().contains('n')}.name", Matchers.hasItems("Maria Joaquina","Ana Julia"))
		;
	}
	
}
