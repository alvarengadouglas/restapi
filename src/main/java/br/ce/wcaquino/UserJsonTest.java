package br.ce.wcaquino;

import java.util.Arrays;

import org.hamcrest.Matchers;
import org.junit.Test;

import io.restassured.RestAssured;

public class UserJsonTest {
	
	
	@Test
	public void validaJsonPrimeiroNivel() {
		RestAssured
		.given()
		.when()
			.get("http://restapi.wcaquino.me/users/1")
		.then()
			.statusCode(200)
			.body("id", Matchers.is(1))
			.body("name", Matchers.containsString("Silva"))
			.body("age", Matchers.greaterThan(18));
	}
	
	@Test
	public void validaJsonSegundoNivel() {
		RestAssured
		.given()
		.when()
			.get("http://restapi.wcaquino.me/users/2")
		.then()
			.statusCode(200)
			.body("id", Matchers.is(2))
			.body("name", Matchers.containsString("Joaquina"))
			.body("endereco.rua", Matchers.is("Rua dos bobos"))
			.body("age", Matchers.lessThan(30))
		;
	}
	
	
	@Test
	public void validaJsonComLista() {
		RestAssured
		.given()
		.when()
			.get("http://restapi.wcaquino.me/users/3")
		.then()
			.statusCode(200)
			.body("id", Matchers.is(3))
			.body("name", Matchers.containsString("Ana"))
			.body("filhos[0].name", Matchers.is("Zezinho"))
			.body("filhos[1].name", Matchers.is("Luizinho"))
		;
	}
	
	
	@Test
	public void deveRetornarErroDeNaoEncontrado() {
		RestAssured
		.given()
		.when()
			.get("http://restapi.wcaquino.me/users/4")
		.then()
			.statusCode(404)
			.body("error", Matchers.is("Usuário inexistente"))
		;	
	}
	
	@Test
	public void validarListaJson() {
		
		RestAssured
		.given()
		.when()
			.get("http://restapi.wcaquino.me/users/")
		.then()
			.statusCode(200)
			.body("name", Matchers.hasSize(3))
			.body("name[0]", Matchers.is("João da Silva"))
			.body("filhos.name", Matchers.hasItem(Arrays.asList("Zezinho", "Luizinho")))
			.body("filhos[2].name[0]", Matchers.is("Zezinho"))
		;
		
	}

}
